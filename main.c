#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS // Suppresses a warning message.
#include <CL/cl.h>


char * GL_source_string[1];
size_t GL_source_size[1];

// *****************************************************************************
// Some functions that might be handy.

// Loads our kernel source code
bool load_kernel(const char * file_name){
  FILE *fp;

  fp = fopen(file_name, "r");
  if(!fp){
    fprintf(stderr, "Failed to load kernel.\n");
    return false;
  }

  fseek(fp, 0, SEEK_END);
  size_t fileSize = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  GL_source_string[0] = (char *) malloc(fileSize);
  GL_source_size[0] = fread(GL_source_string[0], 1, fileSize, fp);

  fclose(fp);
  return true;
}

// Returns true if an error has occured
bool hasErrors(cl_int err){
  if(err != CL_SUCCESS){
    fprintf(stderr, "OpenCL failed with an error: %d\n", err);
    return true;
  }
  return false;
}

// *****************************************************************************
// Our main function

int main(){

  // ***************************************************************************
  // Fetch all available platforms
  cl_uint platformIdCount = 0;
  clGetPlatformIDs(0, NULL, &platformIdCount);

  if(platformIdCount == 0){
    fprintf(stderr, "No OpenCL platform was found!\n");
    return 1;
  } else {
    printf("Found %d platform(s).\n", platformIdCount);
  }

  cl_platform_id platformIds[platformIdCount];
  clGetPlatformIDs(platformIdCount, platformIds, NULL);

  // Print all available platforms
  for(cl_uint i = 0; i < platformIdCount; i++){
    char platform_name[32];
    clGetPlatformInfo(
      platformIds[i],
      CL_PLATFORM_NAME,
      32,
      platform_name,
      NULL);

    printf("\t %d : %s\n", (i + 1), platform_name);
  }

  // ***************************************************************************
  // We actually do not care about which platform we're going to use, so we
  // continue directly by querying the devices for the first platform we've found:
  cl_uint deviceIdCount = 0;
  clGetDeviceIDs(
    platformIds[0],
    CL_DEVICE_TYPE_ALL,
    0,
    NULL,
    &deviceIdCount);

  if(deviceIdCount == 0){
    fprintf(stderr, "No OpenCL devices was found!\n");
    return 1;
  } else {
    printf("Found %d device(s).\n", deviceIdCount);
  }

  cl_device_id deviceIds[deviceIdCount];
  clGetDeviceIDs(
    platformIds[0],
    CL_DEVICE_TYPE_ALL,
    deviceIdCount,
    deviceIds,
    NULL);

  // Print available devices
  for(cl_uint i = 0; i < deviceIdCount; i++){
    char device_name[32];
    clGetDeviceInfo(
      deviceIds[i],
      CL_DEVICE_NAME,
      32,
      device_name,
      NULL);

    printf("\t %d : %s\n", (i + 1), device_name);
  }

  // ***************************************************************************
  // Create our context

  const cl_context_properties contextProperties[] = {
    CL_CONTEXT_PLATFORM, (cl_context_properties) platformIds[0], 0, 0
  };

  cl_int error = CL_SUCCESS;

  cl_context context = clCreateContext(
    contextProperties, deviceIdCount,
    deviceIds, NULL,
    NULL, &error
  );

  if(hasErrors(error)){
    return 1;
  }

  printf("Context created.\n");


  // ***************************************************************************
  // Load our kernel and build our program on device

  if(!load_kernel("./saxpy.cl")){
    fprintf(stderr, "Failed to read .cl file.\n");
    return 1;
  }

  // creates our program from source loaded into our global variables.
  cl_program program = clCreateProgramWithSource(
      context,
      1,
      (const char**) GL_source_string,
      GL_source_size,
      &error);

  if(hasErrors(error)){
    return 1;
  }

  // compiles and links our program on the device
  error = clBuildProgram(
    program,
    deviceIdCount,
    deviceIds,
    NULL, NULL, NULL);

  if(hasErrors(error)){
    return 1;
  }

  // "SAXPY" is the function name in the program declared with the __kernel
  // qualifier.
  cl_kernel kernel = clCreateKernel(program, "SAXPY", &error);

  if(hasErrors(error)){
    return 1;
  }

  // ***************************************************************************
  // Allocate device memory and prepare data

  size_t DATA_SIZE = 10;
  float a[DATA_SIZE], b[DATA_SIZE];

  // Not so random is it?
  for(cl_int i = 0; i < DATA_SIZE; i++){
    a[i] = (float) i;
    b[i] = (float) 0;
  }

  // Allocate memory on the device using the buffer API:
  cl_mem aBuffer = clCreateBuffer(
    context,
    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
    sizeof(float) * (DATA_SIZE),
    a,
    &error);

  if(hasErrors(error)){
    return 1;
  }

  cl_mem bBuffer = clCreateBuffer(
    context,
    CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
    sizeof(float) * (DATA_SIZE),
    b,
    &error);

  if(hasErrors(error)){
    return 1;
  }

  // ***************************************************************************
  // Create our command queue

  cl_command_queue queue = clCreateCommandQueue(
    context, deviceIds[0], 0, &error);

  if(hasErrors(error)){
    return 1;
  }

  // ***************************************************************************
  // This is where we add our computations!

  // Bind arguments to the kernel
  clSetKernelArg(kernel, 0, sizeof(cl_mem), &aBuffer);
  clSetKernelArg(kernel, 1, sizeof(cl_mem), &bBuffer);

  const float two = 2.0;
  clSetKernelArg(kernel, 2, sizeof(float), &two);

  // Tell OpenCL what dimension our work domain has
  const size_t global_work_size[] = {DATA_SIZE, 0, 0};

  // Executes the command queue.
  error = clEnqueueNDRangeKernel(queue, kernel,
    1, // one dimension
    NULL,
    global_work_size,
    NULL,
    0, NULL, NULL);
    
  if(hasErrors(error)){
    return 1;
  }

  clEnqueueReadBuffer(queue, bBuffer, CL_TRUE, 0, DATA_SIZE * sizeof(float), b, 0, NULL, NULL);
  clFinish(queue);

  // print results
  for(size_t i = 0; i < DATA_SIZE; i++){
    printf("%.2f\n", b[i] );
  }

  clReleaseCommandQueue (queue);
  clReleaseMemObject (bBuffer);
  clReleaseMemObject (aBuffer);
  clReleaseKernel (kernel);
  clReleaseProgram (program);
  clReleaseContext (context);

  return 0; // this is the end of the main function.
}
