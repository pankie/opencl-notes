CC 		= gcc
CFLAGS  = -l OpenCL -O2

build : main.o
	$(CC) $(CFLAGS) main.o -o main

main.o : main.c
	$(CC) $(CFLAGS) -c main.c -o main.o